import codecs
import pickle
import random
import re
from collections import Counter
from typing import List

import arabic_reshaper
import numpy as np
import tweepy
from PIL import Image
from bidi.algorithm import get_display
from hazm import *
from wordcloud_fa import WordCloudFa

word_cloud_address = 'tweet.jpg'

# cmaps = [('autumn', 'black'), ('winter', 'black'), ('summer', 'black'), ('Greens', 'black'),
#          ('Blues', 'black'), ('viridis', 'black'), ('magma', 'black'), ('cividis', 'black')]

cmaps = [('autumn', 'black'), ('viridis', '#f5f5f5'), ('plasma', '#f5f5f5'),
         ('magma', '#f5f5f5'), ('hsv', '#f5f5f5'), ('jet', '#f5f5f5'),
         ('RdPu', 'black'), ('Greens', 'black'), ('Blues', 'black'), ('Purples', 'black')
         ]

FONT_PATH = "XTitre.TTF"
fonts = ['1.ttf', '2.ttf', '3.ttf', '4.ttf', '5.ttf', '7.ttf', '8.ttf', '9.ttf']
MASK_PATH = "mask.png"

STOPWORDS_PATH = "stopwords.dat"


def clean_str_list(str_list) -> List[str]:
    hamze = 'ء'
    question_mark = '?'
    unwanted_chars = [hamze, question_mark]
    for c in unwanted_chars:
        str_list = [s.replace(c, '') for s in str_list]
    return str_list


class TfIdf:
    tfidf = None

    @classmethod
    def init_tfidf(cls):
        if cls.tfidf is None:
            if cls.load_tfidf():
                return
            cls.tfidf = {}
            counter = Counter()
            with open('persian_lang_corpus.txt') as f:
                cnt = 0
                line_list = []
                for line in f.readlines():
                    line_list.append(line)
                    cnt += 1
                    if cnt % 100000 == 0:
                        words = boil_str_list(clean_str_list(line_list))
                        line_list = []
                        counter.update(words)
                        print('counter:', cnt)
                        common_words = set([word for word, n in counter.most_common(n=int(1e6))])
                        print('length of common words:', len(common_words))
                        to_remove_words = []

                        for word, count in counter.items():
                            if word not in common_words:
                                to_remove_words.append(word)
                        print('length of to remove words:', len(to_remove_words))
                        for word in to_remove_words:
                            del counter[word]

            for word, freq in counter.items():
                cls.tfidf[word] = 1 / np.log10(freq + 1)
            # just save it for further use
            cls.save_tfidf()

    @classmethod
    def get_tfidf(cls, word: str) -> float:
        cls.init_tfidf()
        if word in cls.tfidf:
            return cls.tfidf[word]
        return 1

    @classmethod
    def save_tfidf(cls):
        with open('tfidf.pickle', 'wb') as handle:
            pickle.dump(cls.tfidf, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load_tfidf(cls):
        try:
            with open('tfidf.pickle', 'rb') as handle:
                cls.tfidf = pickle.load(handle)
                return True
        except FileNotFoundError:
            return False


def boil_str_list(str_list: List[str]) -> List[str]:
    # Normalize words
    tokenizer = WordTokenizer()
    lemmatizer = Lemmatizer()
    normalizer = Normalizer()
    stopwords = set(list(map(lambda w: w.strip(), codecs.open(STOPWORDS_PATH, encoding='utf8'))))
    words = []
    for s in str_list:
        s = re.sub(r"[,.;:?!،()]+", " ", s)
        s = re.sub('[^\u0600-\u06FF]+', " ", s)
        s = re.sub(r'[\u200c\s]*\s[\s\u200c]*', " ", s)
        s = re.sub(r'[\u200c]+', " ", s)
        s = re.sub(r'[\n]+', " ", s)
        s = re.sub(r'[\t]+', " ", s)
        s = re.sub(r'[\d]+', " ", s)
        s = normalizer.normalize(s)
        s = normalizer.character_refinement(s)
        s_words = tokenizer.tokenize(s)
        s_words = [lemmatizer.lemmatize(tweet_word).split('#', 1)[0] for tweet_word in s_words]
        s_words = list(filter(lambda x: x not in stopwords, s_words))
        words.extend(s_words)
    return words


def save_word_cloud(user_name: str, api):
    raw_tweets = []
    for tweet in tweepy.Cursor(api.user_timeline, id=user_name).items():
        if tweet.in_reply_to_screen_name is None or tweet.in_reply_to_screen_name==user_name:
            raw_tweets.append(tweet.text)
    words = boil_str_list(clean_str_list(raw_tweets))
    if len(words) == 0:
        return

    # Build word_cloud
    mask = np.array(Image.open(MASK_PATH))
    colormap_bgcolor = random.sample(cmaps, 1)[0]
    colormap, bgcolor = colormap_bgcolor
    font = random.sample(fonts, 1)[0]
    print('colormap is', colormap)
    print('font is', font)
    word_cloud = WordCloudFa(persian_normalize=False, mask=mask, colormap=colormap,
                             background_color=bgcolor, include_numbers=False,
                             font_path=font, prefer_horizontal=0.9,
                             no_reshape=True, max_words=400, min_font_size=7)
    # word_weight = {}
    # word_counter = Counter(words)
    # for word, count in word_counter.items():
    #     word_weight[get_display(arabic_reshaper.reshape(word))] = count * TfIdf.get_tfidf(word)
    # wc = word_cloud.generate_from_frequencies(frequencies=word_weight)
    clean_string = ' '.join([str(elem) for elem in words])
    clean_string = arabic_reshaper.reshape(clean_string)
    clean_string = get_display(clean_string)
    wc = word_cloud.generate(clean_string)
    image = wc.to_image()
    bg_w, bg_h = image.size
    back = Image.open('nudge.png')
    img_w, img_h = back.size
    offset = (bg_w - img_w, bg_h - img_h)
    image.paste(back, offset, back)
    from PIL import ImageEnhance
    image = ImageEnhance.Contrast(image).enhance(1.5)
    image = ImageEnhance.Sharpness(image).enhance(1.2)
    image.save(word_cloud_address)


if __name__ == '__main__':
    from api import api

    save_word_cloud('miladaghajohari', api)
